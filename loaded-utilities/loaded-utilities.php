<?php

/**
 *
 * @link              https://www.loadedcommunications.com.au
 * @since             1.0.4
 * @package           Loaded_Utilities
 *
 * @wordpress-plugin
 * Plugin Name:       Loaded Utilities
 * Plugin URI:        https://www.loadedcommunications.com.au
 * Description:       Collection of utility functions and admin customisation for Loaded websites.
 * Version:           1.0.4
 * Author:            Loaded Communications
 * Author URI:        https://www.loadedcommunications.com.au
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       loaded-utilities
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
  die;
}

require( __DIR__ . '/vendor/autoload.php');

use Leafo\ScssPhp\Compiler;
use SSNepenthe\ColorUtils\Colors\Rgba as R;
use SSNepenthe\ColorUtils\Colors\Color as C;
use SSNepenthe\ColorUtils\Colors\ColorFactory;
use function SSNepenthe\ColorUtils\{
    alpha, blue, brightness, brightness_difference, color, color_difference,
    contrast_ratio, green, hsl, hsla, hue, is_bright, is_light, lightness,
    looks_bright, name, opacity, perceived_brightness, red, relative_luminance, rgb,
    rgba, saturation, lighten
};
use function lewiscowles\Utils\FileSystem\Extension\fixExtensionIfNeeded;
use PhilipNewcomer\ACF_Unique_ID_Field;

/**
 * Currently plugin version.
 */

define('LOADED_UTILITIES_VERSION', '1.0.4');
define('ACFGFS_API_KEY', 'AIzaSyBGloJDZahHpjKXY_UyYMKxrk_bN_H248w');

$plugin_dir = WP_PLUGIN_DIR . '/loaded-utilities';
$plugin_url = WP_PLUGIN_URL . '/loaded-utilities';

if ( version_compare( PHP_VERSION, '7', '<' ) ) {
  ?>
  <div id="error-page">
      <p>This plugin requires PHP 7 or higher. Please contact your hosting provider about upgrading your
          server software. Your PHP version is <b><?php echo PHP_VERSION; ?></b></p>
  </div>
  <?php
  die();
}

function activate_loaded_utilities() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-activator.php';
  Loaded_Utilities_Activator::activate();
}

function deactivate_loaded_utilities() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-deactivator.php';
  Loaded_Utilities_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_loaded_utilities');
register_deactivation_hook(__FILE__, 'deactivate_loaded_utilities');

require plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.4
 */

if ( function_exists('get_field') ) {
  if ( get_field( 'filename', 'option' ) ) {
    $site_slug = strtolower(get_field( 'filename', 'option' ));
  } 
  if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
    $site_slug = strtolower(basename( get_bloginfo('url') ));
  }
} else {
  $site_slug = strtolower(basename( get_bloginfo('url') ));
}

$loaded_utilities_includes[] = '/admin-head-init.php';
$loaded_utilities_includes[] = '/enable-svg-support.php';

if ( $site_slug !== 'parent' ) {
  $loaded_utilities_includes[] = '/generate-theme-scss.php';
}

foreach ( $loaded_utilities_includes as $file ) {
  $filepath = $plugin_dir . '/inc' . $file;
  if ( ! $file ) {
    trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
  }
  require_once $filepath;
}

function run_loaded_utilities() {
  $plugin = new Loaded_Utilities();
  $plugin->run();
}

run_loaded_utilities();


?>

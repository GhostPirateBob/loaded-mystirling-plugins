<?php 

function set_default_admin_color($user_id) {
  $args = array(
    'ID'          => $user_id,
    'admin_color' => 'flat',
  );
  wp_update_user($args);
}
add_action('user_register', 'set_default_admin_color');

function deregister_jquery_migrate( $scripts ) {
  if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
    $jquery_dependencies = $scripts->registered['jquery']->deps;
    $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
  }
}

add_action( 'wp_default_scripts', 'deregister_jquery_migrate' );

// function loaded_login_style() {
//   if ( function_exists('get_field') ) {
//     if ( get_field( 'filename', 'option' ) ) {
//       $site_slug = strtolower(get_field( 'filename', 'option' ));
//     } 
//     if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
//       $site_slug = strtolower(basename( get_bloginfo('url') ));
//     }
//   }
//   if (!function_exists('get_field')) {
//     $site_slug = strtolower(basename( get_bloginfo('url') ));
//   }
//   if (file_exists( WP_PLUGIN_DIR . '/loaded-utilities/admin/css/admin-style-' . $site_slug . '.css')) {
//     wp_enqueue_style( 'mystirling-login-styles', WP_PLUGIN_URL . '/loaded-utilities/admin/css/admin-style-' . $site_slug . '.css', array(), '1.0.4', true );
//   }
// }

// add_action( 'login_enqueue_scripts', 'loaded_login_style', 10 );

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
  $post_types = get_post_types();
  foreach ($post_types as $post_type) {
    if (post_type_supports($post_type, 'comments')) {
      remove_post_type_support($post_type, 'comments');
      remove_post_type_support($post_type, 'trackbacks');
    }
  }
}

add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
  return false;
}

add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
  $comments = array();
  return $comments;
}

add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
  remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
  global $pagenow;
  if ($pagenow === 'edit-comments.php') {
    wp_redirect(admin_url());exit;
  }
}

add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}

add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
  if (is_admin_bar_showing()) {
    remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  }
}

add_action('init', 'df_disable_comments_admin_bar');

class loaded_color_schemes {

  private $__colors = array(
    'primary', 'flat',
  );

  public function __construct() {
    add_action('admin_init', array($this, 'add_colors'));
  }

  public function add_colors() {
    $suffix = is_rtl() ? '-rtl' : '';
    if ( function_exists('get_field') ) {
      if ( get_field( 'filename', 'option' ) ) {
        $site_slug = strtolower(get_field( 'filename', 'option' ));
      }
      if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }
    }
    if (!function_exists('get_field')) {
      $site_slug = strtolower(basename( get_bloginfo('url') ));
    }
    wp_admin_css_color(
      'flat', __('Flat', 'admin_schemes'),
      plugins_url("../admin/css/admin-style-" . $site_slug . ".css", __FILE__),
      array('#141f27', '#202a32', '#24c7c4', '#fd7e14'),
      array('base' => '#f1f2f3', 'focus' => '#fff', 'current' => '#fff')
    );
  }
}

global $acs_colors;
$acs_colors = new loaded_color_schemes;
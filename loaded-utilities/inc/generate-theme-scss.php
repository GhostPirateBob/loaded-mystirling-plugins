<?php
$plugin_dir = WP_PLUGIN_DIR . '/loaded-utilities';
$plugin_url = WP_PLUGIN_URL . '/loaded-utilities';

require( $plugin_dir . '/vendor/autoload.php');

use Leafo\ScssPhp\Compiler;
use SSNepenthe\ColorUtils\Colors\Rgba as R;
use SSNepenthe\ColorUtils\Colors\Color as C;
use SSNepenthe\ColorUtils\Colors\ColorFactory;
use function SSNepenthe\ColorUtils\{
    alpha, blue, brightness, brightness_difference, color, color_difference,
    contrast_ratio, green, hsl, hsla, hue, is_bright, is_light, lightness,
    looks_bright, name, opacity, perceived_brightness, red, relative_luminance, rgb,
    rgba, saturation, lighten
};
use function lewiscowles\Utils\FileSystem\Extension\fixExtensionIfNeeded;
use PhilipNewcomer\ACF_Unique_ID_Field;

function save_post_generate_scss( $post_id ) {

$plugin_dir = WP_PLUGIN_DIR . '/loaded-utilities';
$plugin_url = WP_PLUGIN_URL . '/loaded-utilities';

// $values = $_POST['acf'];
// global $post;
// update_field('debug',  json_encode($currentTheme), 'option');

global $post;

if (function_exists('acf_add_options_page')) {
  if ( $post_id === 'options' ) {

    $wp_content_path = WP_CONTENT_DIR;
    $wp_content_url = WP_CONTENT_URL;
    $site_url = get_bloginfo('url');
    $theme_dir = get_stylesheet_directory();
    $theme_url = get_stylesheet_directory_uri();

    if ( function_exists('get_field') ) {
      if ( get_field( 'filename', 'option' ) ) {
        $site_slug = strtolower(get_field( 'filename', 'option' ));
      } 
      if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }
    }
    if (!function_exists('get_field')) {
      $site_slug = strtolower(basename( get_bloginfo('url') ));
    }

    $theme_css_path =     $theme_dir  . '/css/theme-' . $site_slug . '.css';
    $theme_scss_path =    $theme_dir  . '/sass/theme.scss';
    $theme_css_url =      $theme_url  . '/css/theme-' . $site_slug . '.css';
    $theme_colors_path =  $theme_dir  . '/sass/theme/_theme-colors-' . $site_slug . '.scss';
    $admin_css_path =     $plugin_dir . '/admin/css/admin-style-' . $site_slug . '.css';
    $admin_scss_path =    $plugin_dir . '/admin-style.scss';
    $admin_css_url =      $plugin_dir . '/admin/css/admin-style-' . $site_slug . '.css';
    $admin_colors_path =  $plugin_dir . '/_theme-colors-' . $site_slug . '.scss';

    $primary = get_field( 'primary', 'option' );
    $secondary = get_field( 'secondary', 'option' );
    $light = get_field( 'light', 'option' );
    $dark = get_field( 'dark', 'option' );
    $mystirling_blue = get_field( 'mystirling_blue', 'option' );

    $heading_font = get_field( 'heading_font', 'option' );
    $body_font = get_field( 'body_font', 'option' );
    $button_font = get_field( 'button_font', 'option' );


    $font_size_base = get_field( 'font_size_base', 'option' );
    $font_size_base = $font_size_base . 'rem';
    $font_weight_normal = get_field( 'font_weight_normal', 'option' );
    $font_weight_headings = get_field( 'font_weight_headings', 'option' );
    $line_height_headings = get_field( 'line_height_headings', 'option' );
    $line_height_body = get_field( 'line_height_body', 'option' );
    $h1_font_size = get_field( 'h1_font_size', 'option' );
    $h1_font_size = $h1_font_size . 'rem';
    $h2_font_size = get_field( 'h2_font_size', 'option' );
    $h2_font_size = $h2_font_size . 'rem';
    $h3_font_size = get_field( 'h3_font_size', 'option' );
    $h3_font_size = $h3_font_size . 'rem';

    $body_bg = get_field( 'body_bg', 'option' );
    $body_color = get_field( 'body_color', 'option' );
    $footer_color = get_field( 'footer_color', 'option' );
    $footer_heading_color = get_field( 'footer_heading_color', 'option' );
    $gmap_color = get_field( 'gmap_color', 'option' );
    $gmap_heading_color = get_field( 'gmap_heading_color', 'option' );

    $bg_pattern_one = get_field( 'bg_pattern_one', 'option' );
    $bg_pattern_two = get_field( 'bg_pattern_two', 'option' );
    $bg_pattern_one = $bg_pattern_one['url'];
    $bg_pattern_two = $bg_pattern_two['url'];

    $enable_gradient_bg = get_field('enable_gradient_bg', 'option');
    $bg_nav_right_enquire_submit = get_field( 'bg_nav_right_enquire_submit', 'option' );
    $bg_nav_right = get_field( 'bg_nav_right', 'option' );
    $nav_right_button = get_field( 'nav_right_button', 'option' );
    $nav_right_menu_item = get_field( 'nav_right_menu_item', 'option' );
    $nav_right_menu_item_hover = get_field( 'nav_right_menu_item_hover', 'option' );

    $footer_area_bg = get_field('footer_area_bg', 'option');
    $next_page_background = get_field('next_page_background', 'option');
    $next_page_button_text = get_field('next_page_button_text', 'option');
    $next_page_button_text_hover = get_field('next_page_button_text_hover', 'option');
    $next_page_button_background = get_field('next_page_button_background', 'option');

    if ( $heading_font['font'] == 'Montserrat' ) {
      $heading_font['font'] = 'Montreal';
    }

    $logo = "";
    if ( get_field('logo', 'option') ) {
      $logo = get_field('logo', 'option');
      $logo_url = $site_url . $logo['url'];
      $logo_id = $logo['id'];
      $logo_path = $wp_content_path . str_replace('/wp-content', '', $logo['url']);
      $logo_path = str_replace($site_url, '', $logo_path);
      $logo_output_path = $theme_dir . '/img/_' . $site_slug . '_' . $logo['filename'];
      $logo_file = file_get_contents($logo_path);
      file_put_contents( $logo_output_path, $logo_file );
    }

$theme_colors = '
$black: #000 !default;
$white: #fff !default;

$primary-input: ' . $primary . ' !default;
$primary: $primary-input !default;
$primary-light: scale-color($primary, $lightness: 5%) !default;
$primary-lighter: scale-color($primary, $lightness: 10%) !default;
$primary-dark: scale-color($primary, $lightness: -5%) !default;
$primary-darker: scale-color($primary, $lightness: -10%) !default;
$primary-active: rgba($primary, 0.92) !default;
$primary-inactive: rgba($primary, 0.72) !default;

$secondary-input: ' . $secondary . ' !default;
$secondary: $secondary-input !default;
$secondary-light: scale-color($secondary, $lightness: 5%) !default;
$secondary-lighter: scale-color($secondary, $lightness: 10%) !default;
$secondary-dark: scale-color($secondary, $lightness: -5%) !default;
$secondary-darker: scale-color($secondary, $lightness: -10%) !default;
$secondary-active: rgba($secondary, 0.92) !default;
$secondary-inactive: rgba($secondary, 0.72) !default;

$bg-nav-right-input: ' . $bg_nav_right . ' !default;
$bg-nav-right: $bg-nav-right-input !default;
$bg-nav-right-enquire-submit: ' . $bg_nav_right_enquire_submit . ' !default;

$nav-right-button: ' . $nav_right_button . ' !default;
$bg-nav-right: ' . $bg_nav_right . ' !default;
$nav-right-menu-item: ' . $nav_right_menu_item . ' !default;
$nav-right-menu-item-hover: ' . $nav_right_menu_item_hover . ' !default;

$nav-right-button-light: scale-color($nav-right-button, $lightness: 12%) !default;
$nav-right-button-dark: scale-color($nav-right-button, $lightness: -5%) !default;
$nav-right-button-darker: scale-color($nav-right-button, $lightness: -10%) !default;

';

if ( $enable_gradient_bg === 'yes' ) {
  $theme_colors .= '$bg-gradient-nav: linear-gradient(-162deg, $nav-right-button-light 35%, $nav-right-button 60%, $nav-right-button-dark 80%, $nav-right-button-darker 100%) !default;';
}
if ( $enable_gradient_bg === 'no' || $enable_gradient_bg === false ) {
  $theme_colors .= '$bg-gradient-nav: $nav-right-button !default;';
}

$theme_colors .= '
$footer-area-bg: ' . $footer_area_bg . ' !default;
$next-page-background: ' . $next_page_background . ' !default;
$next-page-button-text: ' . $next_page_button_text . ' !default;
$next-page-button-text-hover: ' . $next_page_button_text_hover . ' !default;
$next-page-button-background: ' . $next_page_button_background . ' !default;

$light-input: ' . $light . ' !default;
$light: $light-input !default;

$dark-input: ' . $dark . ' !default;
$dark: $dark-input !default;

$darker-input: scale-color($dark, $lightness: -10%) !default;
$darker: $darker-input !default;

$text-muted: rgba($light, .72) !default;

$mystirling-blue-input: ' . $mystirling_blue . ' !default;
$mystirling-blue: $mystirling-blue-input !default;
$mystirling-light: scale-color($mystirling-blue, $lightness: 5%) !default;
$mystirling-blue-dark: scale-color($mystirling-blue, $lightness: -5%) !default;
$mystirling-blue-darker: scale-color($mystirling-blue, $lightness: -10%) !default;

$font-family-sans-serif: "' . $body_font['font'] . '", Helvetica, Arial, sans-serif !default;
$font-family-heading : "' . $heading_font['font'] . '", Helvetica, Arial, sans-serif !default;

$text-black-color:  $' . $body_color . ' !default;
$text-body-color:   $' . $body_color . ' !default;
$text-muted-color:  rgba($' . $body_color . ', 0.72) !default;

$body-bg: $' . $body_bg . ' !default;
$body-color: $' . $body_color . ' !default;
$footer-color: $' . $footer_color . ' !default;
$footer-heading-color: $' . $footer_heading_color . ' !default;
$gmap-color: $' . $gmap_color . ' !default;
$gmap-heading-color: $' . $gmap_heading_color . ' !default;

$bg-texture-one: url("' . $bg_pattern_one . '") !default;
$bg-texture-two: url("' . $bg_pattern_two . '") !default;

$bg-login-logo: url("' . $logo_url  . '") !default;

$font-size-base: ' . $font_size_base . ' !default;
$font-weight-normal: ' . $font_weight_normal . ' !default;

$headings-font-weight: ' . $font_weight_headings . ' !default;
$headings-line-height: ' . $line_height_headings . ' !default;
$body-line-height: ' . $line_height_body . ' !default;

$h1-font-size: ' . $h1_font_size . ' !default;
$h2-font-size: ' . $h2_font_size . ' !default;
$h3-font-size: ' . $h3_font_size . ' !default;
';

    file_put_contents($theme_colors_path, $theme_colors);
    file_put_contents($admin_colors_path, $theme_colors);

    chdir( $theme_dir . '/sass' );
    $scss = new Compiler();

    if ( file_exists('./theme.scss') && file_exists($theme_colors_path) ) {
      $themeSCSS = "@import \"./theme/_theme-colors-" . $site_slug . ".scss\";\n"
      . file_get_contents( './theme.scss' );
      $returnSCSS = $scss->compile($themeSCSS);
      file_put_contents( $theme_css_path, $returnSCSS );
    }

      chdir( $plugin_dir );
      $scssAdmin = new Compiler();

    if ( file_exists('./admin-style.scss') && file_exists($admin_colors_path) ) {
      $themeSCSSAdmin = "@import \"./_theme-colors-" . $site_slug . ".scss\";\n"
      . file_get_contents( './admin-style.scss' );
      $returnSCSSAdmin = $scssAdmin->compile( $themeSCSSAdmin );
      file_put_contents( $admin_css_path, $returnSCSSAdmin );
    }
  }
}
}

add_action('acf/save_post', 'save_post_generate_scss', 15);

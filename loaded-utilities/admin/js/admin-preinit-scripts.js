console.log('loaded preinit admin scripts - start');

jQuery('.section-container-collapse').each(function (index, element) {
  console.log('creating section-collapse-'+index);
  jQuery(this).attr('id', 'section-collapse-'+index);
  if ( jQuery(this).parents('.acf-field-clone').length > 0 ) {
    if ( jQuery(this).parents('.acf-field-clone').hasClass('section-container-apartments') ) {
      jQuery(this).addClass('show');
    }
  }
  else {
    jQuery(this).parents('.layout').find('.acf-fc-layout-controls').append('<button class="btn btn-success acf-js-tooltip" title="Section Design" type="button" data-toggle="collapse" data-target="#section-collapse-'+index+'"><i class="fa fa-paint-brush"></i></button>');
    jQuery(this).wrapAll('<div class="d-block w-100 collapse-wrapper" />');
  }
});

// if ( jQuery('.acf-field.acf-field-smart-button').length > 0 ) {
//   jQuery('.acf-field.acf-field-smart-button').each(function (index, element) {
//     jQuery(this).find('.acf-input').addClass('collapse').attr('id', 'smartButton'+index);
//     jQuery(this).find('.acf-label > label').append('<button class="btn btn-primary btn-open-section acf-js-tooltip" title="Open Section" type="button" data-toggle="collapse" data-target="#smartButton'+index+'"><i class="fa fa-folder-open-o"</button>'); 
//   });
// }

if ( jQuery('.acf-field.section-container-custom-background-color').length > 0 ) {
  jQuery('.acf-field.section-container-custom-background-color > .acf-input > .acf-button-group > label > input').each(function (index, element) {
    var currentVal = jQuery(this).attr('value');
    jQuery(this).parent().addClass(currentVal);
  });
}

if ( jQuery('.acf-button-group input[value="none"]').length > 0 ) {
  jQuery('.acf-button-group input[value="none"]').each(function (index, element) {
    jQuery(this).parent().addClass('acf-button-none');
  });
}

if ( jQuery('.acf-button-group input[value="bg-dark"]').length > 0 ) {
  jQuery('.acf-button-group input[value="bg-dark"]').each(function (index, element) {
    jQuery(this).parent().addClass('acf-button-bg-dark');
  });
}

if ( jQuery('#bgPatterns').length > 0 ) {
  if ( jQuery('#bgPatterns').attr('data-bg-pattern-one') ) {
    if ( jQuery('#bgPatterns').attr('data-bg-pattern-one').length > 5 ) {
      jQuery('.section-container-custom-background-texture .acf-button-group input[value="bg-pattern-one"]')
        .parent().append('<img src="'+jQuery('#bgPatterns').attr('data-bg-pattern-one')+'" class="texture-img" />');
    }
  }
  if ( jQuery('#bgPatterns').attr('data-bg-pattern-two') ) {
    if ( jQuery('#bgPatterns').attr('data-bg-pattern-two').length > 5 ) {
      jQuery('.section-container-custom-background-texture .acf-button-group input[value="bg-pattern-two"]')
        .parent().append('<img src="'+jQuery('#bgPatterns').attr('data-bg-pattern-two')+'" class="texture-img" />');
    }
  }
}

if ( jQuery('.acf-field-medium-editor').length > 0 ) {
  jQuery('.acf-field-medium-editor').each(function (index, element) {
    var dataName = jQuery(this).find('textarea').attr('name');
    console.log('.acf-field-medium-editor: '+dataName);
    jQuery(this).attr('data-flexi-id', dataName);
  });
}

if ( jQuery('body').hasClass('wp-admin') && 
      jQuery('body').hasClass('post-php') && 
      jQuery('body').hasClass('post-type-page') && 
      jQuery('.acf-field-flexible-content').length > 0 ) {
        jQuery('#wpfooter').before('<div id="app" class="edit-page position-right"><loaded-color-picker></loaded-color-picker></div>');
}

console.log('loaded preinit admin scripts - completed');


console.log('loaded admin scripts - start');
jQuery( document ).ready( function () {
  if ( jQuery( '#tsf-inpost-box' ).length > 0 ) {
    if ( jQuery( '#tsf-inpost-box' ).hasClass( 'closed' ) === false ) {
      jQuery( '#tsf-inpost-box' ).addClass( 'closed' );
    }
  }
  if ( jQuery( '.editableform .editable-checklist' ).length > 0 ) {
    jQuery( '.editableform .editable-checklist > div > label > span' ).each( function ( index ) {
      var labelText = jQuery( this ).html();
      var labelDecoded = jQuery( '<textarea />' ).html( labelText ).text();
      jQuery( this ).html( labelDecoded );
    } );
  }
  if ( jQuery( '#wpbody-content .updated' ).length > 0 ) {
    jQuery( '#wpbody-content .updated' ).each( function ( index, element ) {
      var wrapContent = jQuery( this ).html();
      var wrapResult1 = wrapContent.match( /ExactMetrics/gim );
      var wrapResult2 = wrapContent.match( /Menu Editor/gim );
      var wrapResult3 = wrapContent.match( /Licence/gim );
      var wrapResult4 = wrapContent.match( /Key/gim );
      if ( wrapResult1 || wrapResult2 || wrapResult3 || wrapResult4 ) {
        jQuery( this ).hide();
      }
      else {
        jQuery( this ).addClass( 'show' );
      }
    } );
  }
  if ( typeof acf !== 'undefined' ) {
    var instance = new acf.Model( {
      events: {
        'change': 'onChange',
        'change input[type="radio"]': 'onChangeRadio'
      },
      onChange: function ( e ) {
        console.log('acf onChange');
        var target = e.target.id;
        console.log({target: e});
        var layer_id = null;
        var svg_map_id = null;
      },
      onChangeRadio: function ( e ) {
        var target = e.target.id;
        var value = e.target.value;
        var name = e.target.name;

        // console.log( {
        //   message: "acf onChangeRadio",
        //   onChangeRadioTarget: e,
        //   target: target,
        //   saves: saves, 
        //   id: value, 
        //   this: $this
        // } );
        if (typeof saves !== "undefined") {
          if ( saves.hasOwnProperty(e.target.value)) {
              jQuery('.acf-field.acf-field-text.preview-svg input[type="text"]').val(saves[ e.target.value ].image);
              jQuery('.acf-field.acf-field-text.preview-svg img').attr( 'src', saves[ e.target.value ].image ).removeClass( 'd-none' );
          }
        }
        if ( value.length > 0 && jQuery( 'input[name="' + name + '"]' ).length > 0 ) {
          if ( jQuery( 'input[name="' + name + '"]' ).parents('.acf-field-button-group').hasClass( 'section-container-custom-background-color' ) ) {
            jQuery( 'input[name="' + name + '"]' ).parents( '.layout' ).attr( 'data-section-bg', value );
          }
          if ( jQuery( 'input[name="' + name + '"]' ).parents('.acf-field-button-group').hasClass( 'heading-field-heading-color' ) ) {
            jQuery( 'input[name="' + name + '"]' ).parents( '.layout' ).find( '.acf-smart-button-fields input' ).attr( 'data-input-color',
              value );
          }
          if ( jQuery( 'input[name="' + name + '"]' ).parents('.acf-field-button-group').hasClass( 'button-field-theme-color' )  &&
                jQuery( 'input[name="' + name + '"]' ).parents('.acf-field-button-group').attr('data-name') === 'button_color') {
            jQuery( 'input[name="' + name + '"]' ).parents( '.layout' ).find( '.acf-smart-button-fields input' ).attr( 'data-input-color',
              value );
          }
        }
      }
    } );
  }
  if ( jQuery( '#app' ).length > 0 ) {
    new Vue( {
      components: {
        loadedColorPicker
      },
      data: {
        pickerOpen: false,
        hexSelected: '#24c7c4',
        rgbaSelected: null,
        fieldSelected: null,
        rgbaInputACF: document.querySelectorAll( '.minicolors' ),
        instanceObject: instance,
        acfObject: acf
      },
      methods: {
        pickerOpenListener: function ( evt ) {
          e.preventDefault();
          this.pickerOpen = true;
          // console.log(evt);
          // console.log( 'pickerOpenListener(\'click\')' );
          this.hexSelected = jQuery( this ).find( '.minicolors-input' ).val();
          this.rgbaSelected = jQuery( this ).find( '.rgbatext' ).val();
          this.inputValueRgba = colorConvert.parse( this.inputValueRgbaString );
          this.fieldSelected = jQuery( this ).parents( '.acf-field.acf-field-rgba-color' ).attr( 'data-key' );
          // console.log(this.hexSelected);
          // console.log(this.rgbaSelected);
          // console.log(this.inputValueRgba);
          // console.log(this.fieldSelected);
        }
      },
      created: function () {
        this.rgbaInputACF.forEach( function ( rgbaInput ) {
          rgbaInput.addEventListener( 'mousedown', this.pickerOpenListener );
        } );
        // console.log(this.rgbaInputACF);
      },
      mounted: function () {
        // console.log('mounted');
        // console.log(acf);
        // console.log(this.acfObject);
        // console.log(this.instanceObject);

      },
      destroyed: function () {
        this.rgbaInputACF.forEach( function ( rgbaInput ) {
          rgbaInput.removeEventListener( 'click', this.pickerOpenListener );
        } )
      }
    } ).$mount( '#app' )
  }
  jQuery('.acf-button-group.-vertical input[value="no"]').each( function ( index, element ) {
    jQuery(this).parent().addClass('radio-negative');
  });
  jQuery( '.section-container-custom-background-color > .acf-input > .acf-button-group > label' ).each( function ( index, element ) {
    if ( jQuery( this ).hasClass( 'selected' ) ) {
      var initialClass = jQuery( this ).attr( 'class' );
      initialClass = initialClass.replace( 'selected', '' );
      initialClass = initialClass.replace( 'acf-button-bg-dark', '' );
      initialClass = initialClass.replace( /\s*/, '' );
      initialClass = initialClass.replace( ' ', '' );
      jQuery( this ).parents( '.layout' ).attr( 'data-section-bg', initialClass );
    }
  } );
  jQuery( '.heading-field-heading-color > .acf-input > .acf-button-group > label' ).each( function ( index, element ) {
    if ( jQuery( this ).hasClass( 'selected' ) ) {
      var headingClass = jQuery( this ).find('input').val();
      headingClass = headingClass.replace( 'selected', '' );
      headingClass = headingClass.replace( 'acf-button-bg-dark', '' );
      headingClass = headingClass.replace( /\s*/, '' );
      headingClass = headingClass.replace( ' ', '' );
      jQuery( this ).parents( '.layout' ).attr( 'data-heading-color', headingClass );
      jQuery( this ).parents( '.layout' ).find( '.acf-smart-button-fields input' ).attr( 'data-input-color', headingClass );
    }
  } );
  jQuery( '.button-field-theme-color[data-name="button_color"] > .acf-input > .acf-button-group > label' ).each( function ( index, element ) {
    if ( jQuery( this ).hasClass( 'selected' ) ) {
      var headingClass = jQuery( this ).find('input').val();
      headingClass = headingClass.replace( 'selected', '' );
      headingClass = headingClass.replace( 'acf-button-bg-dark', '' );
      headingClass = headingClass.replace( /\s*/, '' );
      headingClass = headingClass.replace( ' ', '' );
      jQuery( this ).parents( '.layout' ).attr( 'data-heading-color', headingClass );
      jQuery( this ).parents( '.layout' ).find( '.acf-smart-button-fields input' ).attr( 'data-input-color', headingClass );
    }
  } );
  jQuery( '.button-field-theme-color[data-name="button_color"] > .acf-input > .acf-button-group > label' ).each( function ( index, element ) {
    if ( jQuery( this ).hasClass( 'selected' ) ) {
      var headingClass = jQuery( this ).find('input').val();
      headingClass = headingClass.replace( 'selected', '' );
      headingClass = headingClass.replace( 'acf-button-bg-dark', '' );
      headingClass = headingClass.replace( /\s*/, '' );
      headingClass = headingClass.replace( ' ', '' );
      jQuery( this ).parents( '.layout' ).attr( 'data-heading-color', headingClass );
      jQuery( this ).parents( '.layout' ).find( '.acf-smart-button-fields input' ).attr( 'data-input-color', headingClass );
    }
  } );
  if ( jQuery( '.gmap-static-img' ).length > 0 ) {
    jQuery('.gmap-static-img').each(function (index, element) {
      jQuery(this).attr('disabled', 'disabled').prop('disabled', true);
      var inputURL = jQuery(this).find('input[type="url"]').val();
      var inputURLTest = inputURL.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        if ( inputURLTest !== null ) {
          jQuery(this).append('<img class="gmap-static-img-preview img-fluid mt-3 mb-0" src="'+inputURL+'" />');
        }
    });
  }
});

<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.loadedcommunications.com.au
 * @since      1.0.0
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/admin
 * @author     Loaded Communications <webmaster@loadedcommunications.com.au>
 */
class Loaded_Utilities_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
    if ( function_exists('get_field') ) {
      if ( get_field( 'filename', 'option' ) ) {
        $site_slug = strtolower(get_field( 'filename', 'option' ));
      } 
      if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }
    }
    if (!function_exists('get_field')) {
      $site_slug = strtolower(basename( get_bloginfo('url') ));
    }
    // if ( is_admin() ) {
    //   wp_enqueue_style( 'admin-style', plugin_dir_url( __FILE__ ) . 'css/admin-style-' . $site_slug . '.css', array(), $this->version, 'all' );
    // }
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

    $screen = get_current_screen();

    if ( function_exists('get_field') ) {
      if ( get_field( 'filename', 'option' ) ) {
        $site_slug = strtolower(get_field( 'filename', 'option' ));
      } 
      if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }
      $svg_maps = array();
      $levels = array();
      if (get_field('floorplates_svg_map', 'option') && get_option('image-map-pro-wordpress-admin-options')) {
        $floorplates_svg_map = get_field('floorplates_svg_map', 'option');
        $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');
        $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
        $svg_image_map_meta_saves = $svg_image_map_meta['saves'];
        $svg_saves = array();
        foreach ( $svg_image_map_meta_saves as $save ) { 
          $decoded_save = sanitize_json_for_save($save);
          $decoded_save = json_decode($decoded_save['json']);
          $svg_saves[$decoded_save->general->shortcode]['id'] = $decoded_save->id;
          $svg_saves[$decoded_save->general->shortcode]['shortcode'] = $decoded_save->general->shortcode;
          $svg_saves[$decoded_save->general->shortcode]['name'] = $decoded_save->general->name;
          $svg_saves[$decoded_save->general->shortcode]['width'] = $decoded_save->general->width;
          $svg_saves[$decoded_save->general->shortcode]['height'] = $decoded_save->general->height;
          $svg_saves[$decoded_save->general->shortcode]['image'] = $decoded_save->image->url;
          if (intval($decoded_save->id) === intval($floorplates_svg_map['value'])) {
            $floorplates_svg_levels = $decoded_save->layers->layers_list;
            foreach ( $floorplates_svg_levels as $level ) {
              $levels[strval($level->id)]['id'] = strval($level->id);
              $levels[strval($level->id)]['title'] = $level->title;
              $levels[strval($level->id)]['image_url'] = $level->image_url;
              $levels[strval($level->id)]['image_width'] = $level->image_width;
              $levels[strval($level->id)]['image_height'] = $level->image_height;
            }
            if (count($levels) > 0) {
              $levels = array_reverse($levels);
            }
          }
        }
      }
    }
    if (!function_exists('get_field')) {
      $site_slug = strtolower(basename( get_bloginfo('url') ));
    }

    // wp_deregister_script('jquery');
    // wp_deregister_script('jquery-migrate');
    // wp_enqueue_script( 'jquery',  plugin_dir_url( __FILE__ ) . 'js/jquery-3.4.1.migrate.min.js', array(), '3.4.1', false );

    wp_enqueue_script( 'admin-preinit-scripts', plugin_dir_url( __FILE__ ) . 'js/admin-preinit-scripts.js', array('jquery'), '1.0.4', true );

    if ( $screen->id === 'page' ) {
      wp_enqueue_script( 'vue', plugin_dir_url( __FILE__ ) . 'js/vue.min.js', array(), '2.6.10', true );
      wp_enqueue_script( 'loaded-color-picker', plugin_dir_url( __FILE__ ) . 'js/loadedColorPicker.umd.js', array(), '1.0.4', true );
    }

    if ( $screen->id === 'page' ) {
      wp_enqueue_script( 'bootstrap-bundle', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array('jquery'), '4.3.1', true );
    }

    wp_register_script( 'admin-scripts', plugin_dir_url( __FILE__ ) . 'js/admin-scripts.js', array(), '1.0.4', true );

    if ( $site_slug !== 'parent') {
      if (isset($levels)) {
        if (count($levels) > 0) {
          wp_localize_script( 'admin-scripts', 'levels', $levels );
        }
      }
      if (isset($svg_saves)) {
        if (count($svg_saves) > 0) {
          wp_localize_script( 'admin-scripts', 'svg_saves', $svg_saves );
        }
      }
    }
    wp_enqueue_script( 'admin-scripts' );
  }
}

<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.loadedcommunications.com.au
 * @since      1.0.4
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.4
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 * @author     Loaded Communications <webmaster@loadedcommunications.com.au>
 */
class Loaded_Utilities_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.4
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'loaded-utilities',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

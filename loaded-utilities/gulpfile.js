var fs = require( 'fs' );
var gulp = require( 'gulp' );
var gutil = require( 'gulp-util' );
var del = require( 'del' );
var flatmap = require( 'gulp-flatmap' );
var gulpif = require( 'gulp-if' );
var exec = require( 'child_process' ).exec;
var notify = require( 'gulp-notify' );
var argv = require( 'yargs' ).argv;
var production = false;
var build = false;
const { watch } = require('gulp');
var tap = require( 'gulp-tap' );
var path = require( 'path' );
var readSync = require( 'read-file-relative' ).readSync;
var header = require( 'gulp-header' );
var cssnano = require( 'gulp-cssnano' );
var rename = require( 'gulp-rename' );
var cfg = require( './gulpconfig.json' );

// sass
var sass = require( 'gulp-sass' );
var postcss = require( 'gulp-postcss' );
var autoprefixer = require( 'gulp-autoprefixer' );
var sourcemaps = require( 'gulp-sourcemaps' );


var beep = function() {
  var os = require( 'os' );
  var file = 'gulp/error.wav';
  if ( os.platform() === 'linux' ) {
    exec( "aplay " + file );
  }
  else {
    console.log( "afplay " + file );
    exec( "afplay " + file );
  }
};

var handleError = function( task ) {
  return function( err ) {
    beep();
    notify.onError( {
      message: task + ' failed, check the logs..',
      sound: false
    } )( err );
    gutil.log( gutil.colors.bgRed( task + ' error:' ), gutil.colors.red( err ) );
  };
};

var tasks = {
  clean: function( cb ) {
    del( [ 'admin/css/*' ], cb );
  },
  sassVerdant: function() {
    return gulp.src( './admin-style.scss' )
    .pipe( header( readSync( cfg.colors.verdant ).toString() ) )
    .pipe( tap( function( file, t ) {
      console.log( 'FILEPATH: ' + file.path );
      return;
    } ) )
    .pipe( ( sourcemaps.init() ) )
    .pipe( sass( {
      outputStyle: "expanded",
      errLogToConsole: true
    } ) )
    .on( 'error', handleError( 'SASS' ) )
    .pipe( rename({ suffix: "-" + cfg.sites.verdant }) )
    .pipe( gulp.dest( 'admin/css/' ) )
    .pipe( cssnano( {
      autoprefixer: {
        browsers: [ '> 1%', 'last 2 versions' ],
        add: true
      }
    } ) )
    .pipe( sourcemaps.write( {
      'includeContent': false,
      'sourceRoot': '../'
    } ) )
    .pipe( rename({ suffix: ".min" }) )
    .pipe( gulp.dest( 'admin/css/' ) );
  },
  sassParent: function() {
    return gulp.src( './admin-style.scss' )
    .pipe( header( readSync( cfg.colors.parent ).toString() ) )
    .pipe( tap( function( file, t ) {
      console.log( 'FILEPATH: ' + file.path );
      return;
    } ) )
    .pipe( ( sourcemaps.init() ) )
    .pipe( sass( {
      outputStyle: "expanded",
      errLogToConsole: true
    } ) )
    .on( 'error', handleError( 'SASS' ) )
    .pipe( rename({ suffix: "-" + cfg.sites.parent }) )
    .pipe( gulp.dest( 'admin/css/' ) )
    .pipe( cssnano( {
      autoprefixer: {
        browsers: [ '> 1%', 'last 2 versions' ],
        add: true
      }
    } ) )
    .pipe( sourcemaps.write( {
      'includeContent': false,
      'sourceRoot': '../'
    } ) )
    .pipe( rename({ suffix: ".min" }) )
    .pipe( gulp.dest( 'admin/css/' ) );
  },
  sassBarque: function() {
  return gulp.src( './admin-style.scss' )
    .pipe( header( readSync( cfg.colors.barque ).toString() ) )
    .pipe( tap( function( file, t ) {
      console.log( 'FILEPATH: ' + file.path );
      return;
    } ) )
    .pipe( ( sourcemaps.init() ) )
    .pipe( sass( {
      outputStyle: "expanded",
      errLogToConsole: true
    } ) )
    .on( 'error', handleError( 'SASS' ) )
    .pipe( rename({ suffix: "-" + cfg.sites.barque }) )
    .pipe( gulp.dest( 'admin/css/' ) )
    .pipe( cssnano( {
      autoprefixer: {
        browsers: [ '> 1%', 'last 2 versions' ],
        add: true
      }
    } ) )
    .pipe( sourcemaps.write( {
      'includeContent': false,
      'sourceRoot': '../'
    } ) )
    .pipe( rename({ suffix: ".min" }) )
    .pipe( gulp.dest( 'admin/css/' ) );
  }
};

// --------------------------
// CUSTOMS TASKS
// --------------------------
gulp.task( 'clean', tasks.clean );
// for production we require the clean method on every individual task

var req = build ? [ 'clean' ] : [];
// individual tasks
gulp.task( 'sassVerdant', req, tasks.sassVerdant );
gulp.task( 'sassBarque', req, tasks.sassBarque );
gulp.task( 'sassParent', req, tasks.sassParent );
// --------------------------
// DEV/WATCH TASK
// --------------------------

gulp.task( 'watch', function() {
  gulp.watch( ['sass/**/*.scss', '!./node_modules/'], [ 'clean', 'sassVerdant', 'sassBarque', 'sassParent' ], function(cb) {
    cb();
  });
  gutil.log( gutil.colors.bgYellow( 'Watching Admin for changes...' ) );
} );

// build task
gulp.task( 'build', [
  'clean',
  'sassVerdant',
  'sassBarque',
  'sassParent'
] );

gulp.task( 'default', [ 'watch' ] );
